require 'net/http'
require 'digest/md5'
require 'fileutils'
require 'archive/tar/minitar'

include Archive::Tar

DIR = File.dirname(__FILE__)
ISO_URL="http://archive.ubuntu.com/ubuntu/dists/precise/main/installer-amd64/current/images/netboot/mini.iso"
ISO_FILENAME=File.basename ISO_URL
ISO_MD5="1278936cb0ee9d9a32961dd7743fa75c"
TEMP_DIR = File.expand_path('temp')
ISO_OUTPUT = File.expand_path(File.join(TEMP_DIR, 'iso'))
MKISOFS = File.join DIR, 'tools', 'mkisofs.exe'
UNZIP_CMD = "tools\\7z"

STDOUT.sync = true

class VagrantLinux
	def execute
		FileUtils.rm_rf TEMP_DIR if Dir.exists? TEMP_DIR
		valid = verify_iso
		download_iso unless valid
		puts "file already downloaded" if valid
		raise "File is corrupt, checksum mismatch, aborting" unless verify_iso
		unzip_iso
		customize_iso
#		author_iso
#		build_box
#		vagrant_box
	end

	def unzip_iso	
		cmd = "#{UNZIP_CMD} x #{ISO_FILENAME} *.* -o\"#{ISO_OUTPUT.gsub("/", "\\")}\""
		puts "Extracting ISO, command line #{cmd}"
		`#{cmd}` #lots of output, suppressing...
		puts "iso unzipped successfully!"
	end

	def download_iso
		puts "downloading image from #{ISO_URL}"
		fetch(ISO_URL) do |response|
			size = 0
			total_size = response.content_length
			File.open(ISO_FILENAME, 'wb') do |f|
				response.read_body do |chunk|
					size += chunk.size
					print "\r"
					print "Downloaded #{size/1048576}MB of #{total_size/1048576}MB"
					f.write chunk
				end
			end
		end
	end

	def verify_iso
		return false if not File.exists? ISO_FILENAME
		file_md5 = Digest::MD5.hexdigest(File.open(ISO_FILENAME, "rb") { |f| f.read })
		if not file_md5 == ISO_MD5
			puts "hashes didn't match. expected #{ISO_MD5}, got #{file_md5}"
			return false
		end
		return true
	end

	#TODO: this probably reads & buffers whole thing, need to check source.  Not a big deal for now.
	def customize_iso
		init_dir = File.join(TEMP_DIR, "init")
		Dir.mkdir init_dir
		original = Zlib::GzipReader.new(File.open(File.join(ISO_OUTPUT, 'initrd.gz'), 'rb'))
      	# Warning: tgz will be closed!
	   	Minitar.unpack(original, init_dir)
  #  	FileUtils.copy_entry 'preseed.cfg', File.join(init_dir, 'initrd', 'preseed.cfg')
	#	tgz = Zlib::GzipWriter.new(File.open('test.gz', 'wb'))
	#	Minitar.pack(init_dir, tgz)
	end

	def fetch(url, limit = 5, &response_block)
		raise "Too many redirects, aborting" if limit == 0
		uri = URI(url)
		http = Net::HTTP.start(uri.host, uri.port) 
		http.request_get(uri.path) do |response|
			case response
		  	when Net::HTTPSuccess then
		  		response_block.call(response)
		  	when Net::HTTPRedirection then
		  		location = response['location']
		    	warn "redirected to #{location}"
		    	fetch(location, limit - 1, &response_block)
		    else
		    	raise "unexpected http response. Response was #{response}"
		    end
		end
	end
end

VagrantLinux.new().execute()